import sys
def determine_state(row, col, slash, backslash):
    state = [[0] * col  for i in range(r)]
    for i, j in slash:
        state[i - 1][j - 1] = 1
    for i, j in backslash:
        state[i - 1][j - 1] = 2
    return state

def has_empty_cell(row, col, state):
    count =0
    for i in range(row):
        for j in range(col):
            if state[i][j] ==0 :
                count+=1
    if count==row * col:
        return True
    return False


def missing_mirror_count(row, col, state):
    count = 0
    pos = None
    for i in range(row - 1):
        for j in range(col- 1):
        count += 1
                pos = (i + 1, j + 1)
    return count, pos


def solve(row, col, m, n, slash, backslash):
    state = determine_state(row, col,slash, backslash)

    if has_empty_cell(row, col, state):
        return "impossible"

    count, pos = missing_mirror_count(row,col ,state)
    if count == 0:
        return "0"
    elif count == 1:
        return f"1 {pos[0]} {pos[1]}"
    else:
        return f"{count} {pos[0]} {pos[1]}"

inp = sys.stdin.readlines()
row,col,m,n = map(int,inp[0].split())
slash = [tuple(map(int,inp[i].split())) for i in range (1,m+1)]
backslash = [tuple(map(int,inp[i].split()))  for i in range(1,n + 1)]
result = solve(row, col, m, n, slash, backslash)
print(result)

